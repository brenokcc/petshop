# -*- coding: utf-8 -*-

"""
PetShop é uma aplicação que visa demonstrar algumas funcionalidades presentes em um sistema de gerenciamento de lojas de animais ou clínicas veterinárias. Na aplicação, funcionários cadastram os dados dos clientes e de seus animais. Através do sistema é possível iniciar tratamentos para determinadas doenças e registrar os procedimentos realizados. Através do painel de controle o funcionário pode acompanhar a quantidade de cadastros realizados ao longo do tempo e visualizar os animais que se encontram em tratamento. Os clientes podem acessar o sistema para consultar os dados de seus animais e acompanhar quanto já foi gasto com tratamento.
"""