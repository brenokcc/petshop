# Generated by Django 2.1.7 on 2019-03-21 14:44

from django.db import migrations
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('petshop', '0006_auto_20180327_1858'),
    ]

    operations = [
        migrations.AddField(
            model_name='cliente',
            name='foto',
            field=djangoplus.db.models.fields.ImageField(default='user.png', null=True, upload_to='alunos', verbose_name='Foto'),
        ),
        migrations.AddField(
            model_name='cliente',
            name='sexo',
            field=djangoplus.db.models.fields.CharField(choices=[['M', 'Masculino'], ['F', 'Feminino']], max_length=255, null=True, verbose_name='Sexo'),
        ),
        migrations.AddField(
            model_name='funcionario',
            name='foto',
            field=djangoplus.db.models.fields.ImageField(default='user.png', null=True, upload_to='alunos', verbose_name='Foto'),
        ),
        migrations.AddField(
            model_name='funcionario',
            name='sexo',
            field=djangoplus.db.models.fields.CharField(choices=[['M', 'Masculino'], ['F', 'Feminino']], max_length=255, null=True, verbose_name='Sexo'),
        ),
    ]
