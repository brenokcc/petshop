# Generated by Django 2.0.3 on 2018-03-27 18:58

from django.db import migrations
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('petshop', '0005_auto_20171003_0903'),
    ]

    operations = [
        migrations.AlterField(
            model_name='animal',
            name='foto',
            field=djangoplus.db.models.fields.ImageField(default='/static/images/blank.png', upload_to='animais', verbose_name='Foto'),
        ),
    ]
