# -*- coding: utf-8 -*-
from datetime import datetime
from djangoplus.db import models
from django.core.exceptions import ValidationError
from djangoplus.decorators import meta, action, subset, role
from pessoas.models import PessoaFisicaAbstrata  #: 4.1


class Doenca(models.Model):  #: 1 Implementando a classe Doença
    descricao = models.CharField(verbose_name='Descrição', search=True, example='Sarna')  #: 1
    contagiosa = models.BooleanField(verbose_name='Contagiosa')  #: 1

    class Meta:  #: 1
        verbose_name = 'Doença'  #: 1 Definindo nome amigável para a classe no singular
        verbose_name_plural = 'Doenças'  #: 1 Definindo nome amigável para a classe no plural
        menu = 'Catálogo::Doenças', 'fa-book'  #: 1 Defindo o menu de acesso para a tela de listagem
        usecase = 'UC001'  #: 1

    def __str__(self):  #: 1 Implementando o método str, o qual será chamado quando um objeto da classe for impresso
        return self.descricao  #: 1


class TipoProcedimento(models.Model):  #: 2 Implementando a classe "Tipo de Procedimento"...
    descricao = models.CharField(verbose_name='Descrição', search=True, example='Banho')  #: 2
    valor = models.DecimalField(verbose_name='Valor', example='23,00')  #: 2

    class Meta:  #: 2 Definindo os meta-atributos...
        verbose_name = 'Tipo de Procedimento'  #: 2
        verbose_name_plural = 'Tipos de Procedimento'  #: 2
        menu = 'Catálogo::Tipos de Procedimento', 'fa-book'  #: 2
        usecase = 'UC002'  #: 2

    def __str__(self):  #: 2 Definindo a função __str__...
        return self.descricao  #: 2


class TipoAnimal(models.Model):  #: 3
    descricao = models.CharField(verbose_name='Descrição', search=True, example='Cão')  #: 3

    class Meta:  #: 3
        verbose_name = 'Tipo de Animal'  #: 3
        verbose_name_plural = 'Tipos de Animais'  #: 3
        menu = 'Catálogo::Tipos de Animais', 'fa-book'  #: 3
        usecase = 'UC003'  #: 3

    def __str__(self):  #: 3
        return self.descricao  #: 3


@role('cpf')  #: 4.1
class Funcionario(PessoaFisicaAbstrata):  #: 4
    """Usuário do sistema responsável pelo cadastro de clientes e animais, #: 4
    bem como o registro dos procedimentos realizados nos tratamentos.""" #: 4
    class Meta:  #: 4
        verbose_name = 'Funcionário'  #: 4
        verbose_name_plural = 'Funcionários'  #: 4
        menu = 'Funcionários', 'fa-user'  #: 4
        usecase = 'UC004'  #: 4


@role('cpf', signup=True)  #: 5.1
class Cliente(PessoaFisicaAbstrata):  #: 5
    """Acessa o sistema para visualizar informações acerca de seus animais"""
    class Meta:  #: 5
        verbose_name = 'Cliente'  #: 5
        verbose_name_plural = 'Clientes'  #: 5
        verbose_female = False  #: 5
        can_admin = 'Funcionário'  #: 5
        can_view_by_role = 'Cliente'  #: 5
        select_display = 'nome', 'cpf'  #: 5
        list_shortcut = True  #: 5
        add_shortcut = True  #: 5
        usecase = 'UC005'  #: 5

    # fieldsets = PessoaFisicaAbstrata.fieldsets + (('Animais', {'relations': ('anial_set',)}),)  #: 5


class AnimalManager(models.DefaultManager):  #: 6.1

    @meta('Meus Animais', can_view=('Cliente',), dashboard='center')  #: 6.1
    def meus_animais(self):  #: 6.1
        return self.all(self._user)  #: 6.1

    @meta('Animais por Tipo', can_view='Funcionário', dashboard='right')  #: 6.1
    def get_qtd_por_tipo(self):  #: 6.1
        return self.all().count('tipo')  #: 6.1

    @meta('Cadastro por Período', can_view='Funcionário', dashboard='center', formatter='bar_chart')  #: 6.1
    def get_qtd_por_periodo(self):  #: 6.1
        return self.all().count('data_cadastro')  #: 6.1


class Animal(models.Model):  #: 6

    nome = models.CharField(verbose_name='Nome', search=True, example='Bob')  #: 6
    tipo = models.ForeignKey(TipoAnimal, verbose_name='Tipo', filter=True, example='Cão')  #: 6

    foto = models.ImageField(verbose_name='Foto', upload_to='animais', sizes=((200, 200),))  #: 6
    descricao = models.TextField(verbose_name='Descrição', example='Animal dócil e bricalhão')  #: 6

    proprietario = models.ForeignKey(Cliente, verbose_name='Proprietário', filter=True, search=('nome',), example='Juca da Silva')  #: 6
    data_cadastro = models.DateField(verbose_name='Data do Cadastro', exclude=True, default=datetime.today)  #: 6

    fieldsets = (  #: 6
        ('Dados Gerais', {'image': 'foto', 'fields': (('nome', 'tipo'), 'data_cadastro', 'descricao', 'proprietario')}),  #: 6
        ('Tratamentos', {'relations': ('tratamento_set',)}),  #: 6
    )  #: 6

    class Meta:  #: 6
        verbose_name = 'Animal'  #: 6
        verbose_name_plural = 'Animais'  #: 6
        can_admin = 'Funcionário'  #: 6
        can_view_by_role = 'Cliente'  #: 6
        list_template = 'image_rows.html'  #: 6
        select_display = 'foto', 'nome', 'tipo'  #: 6
        list_display = 'foto', 'nome', 'tipo', 'descricao'  #: 6
        list_shortcut = 'Funcionário', 'Cliente'  #: 6
        add_shortcut = 'Funcionário'  #: 6
        icon = 'fa-paw'  #: 6
        usecase = 'UC006'  #: 6

        class_diagram = 'procedimento', 'tipoanimal', 'animal', 'tipoprocedimento', 'tratamento'  #: 6

    def __str__(self):  #: 6
        return self.nome  #: 6


class TratamentoManager(models.DefaultManager):  #: 7.1

    @subset('Em Andamento', can_view='Funcionário', can_notify=True)  #: 7.1
    def em_tratamento(self):  #: 7.1
        return self.filter(data_fim__isnull=True)  #: 7.1


class Tratamento(models.Model):  #: 7
    animal = models.ForeignKey(Animal, verbose_name='Animal', composition=True, example='Bob')  #: 7
    doenca = models.ForeignKey(Doenca, verbose_name='Doença', filter=True, example='Sarna')  #: 7
    data_inicio = models.DateField(verbose_name='Data de Início', example='01/01/2018')  #: 7
    data_fim = models.DateField(verbose_name='Data de Término', exclude=True, null=True, example='02/01/2018')  #: 7
    eficaz = models.NullBooleanField(verbose_name='Eficaz', exclude=True, example=True)  #: 7

    fieldsets = (  #: 7.1
        ('Dados Gerais', {'fields': (('animal', 'doenca'),('data_inicio', 'data_fim'))}),  #: 7.1
        ('Procedimentos', {'relations': ('procedimento_set',), 'actions': ('finalizar',)}),  #: 7.1
        ('Resultado', {'fields': ('eficaz',)}),  #: 7.1
    )  #: 7.1

    class Meta:  #: 7
        verbose_name = 'Tratamento'  #: 7
        verbose_name_plural = 'Tratamentos'  #: 7
        add_label = 'Iniciar Tratamento'  #: 7
        can_admin = 'Funcionário'  #: 7
        can_list_by_role = 'Cliente'  #: 7
        list_lookups = 'animal__proprietario',  #: 7
        # list_shortcut = True  #: 7
        usecase = 'UC007'  #: 7

    def pode_ser_finalizado(self):  #: 7.2
        return not self.data_fim and self.procedimento_set.exists()  #: 7.2

    @action('Finalizar Tratamento', can_execute='Funcionário', condition='pode_ser_finalizado', usecase='UC009')  #: 7.2
    def finalizar(self, data_fim, eficaz):  #: 7.2
        if not self.procedimento_set.exists():  #: 7.2
            raise ValidationError('Tratamentos sem procedimentos não podem ser finalizados.')  #: 7.2
        self.data_fim = data_fim  #: 7.2
        self.eficaz = eficaz  #: 7.2
        self.save()  #: 7.2

    def can_edit(self):  #: 7.2
        return not self.procedimento_set.exists()  #: 7.2

    def can_delete(self):  #: 7.2
        return self.can_edit()  #: 7.2


class ProcedimentoManager(models.DefaultManager):  #: 8.1

    @meta('Valor Gasto (R$)', can_view='Cliente', dashboard='right')  #: 8.1
    def get_valor_gasto(self):  #: 8.1
        return self.all(self._user).sum('tipo__valor')  #: 8.1


class Procedimento(models.Model):  #: 8

    tratamento = models.ForeignKey(Tratamento, verbose_name='Tratamento', composition=True)  #: 8

    tipo = models.ForeignKey(TipoProcedimento, verbose_name='Tipo', example='Banho')  #: 8
    data_hora = models.DateTimeField(verbose_name='Data/Hora', example='01/01/2018 10:00')  #: 8

    observacao = models.TextField(verbose_name='Observação', blank=True)  #: 8

    fieldsets = (  #: 8
        ('Dados Gerais', {'fields': ('tratamento', ('tipo', 'data_hora'),)}),  #: 8
        ('Outras Informações', {'fields': ('observacao',)}),  #: 8
    )  #: 8

    class Meta:  #: 8
        verbose_name = 'Procedimento'  #: 8
        verbose_name_plural = 'Procedimentos'  #: 8
        add_label = 'Registrar Procedimento'  #: 8
        can_admin = 'Funcionário'  #: 8
        can_list_by_role = 'Cliente'  #: 8
        list_lookups = 'tratamento__animal__proprietario',  #: 8
        usecase = 'UC008'  #: 8

    def can_add(self):  #: 8
        return not self.tratamento.data_fim  #: 8

    def can_edit(self):  #: 8
        return self.can_add()  #: 8

    def can_delete(self):  #: 8
        return self.can_add()  #: 8
