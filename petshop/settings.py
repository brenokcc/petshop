# -*- coding: utf-8 -*-

from os import sep
from djangoplus.conf.base_settings import *
from os.path import abspath, dirname, join, exists

BASE_DIR = abspath(dirname(dirname(__file__)))
PROJECT_NAME = __file__.split(sep)[-2]

STATIC_ROOT = join(BASE_DIR, 'static')
MEDIA_ROOT = join(BASE_DIR, 'media')

DROPBOX_TOKEN = '' # disponível em https://www.dropbox.com/developers/apps

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(BASE_DIR, 'sqlite.db'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

WSGI_APPLICATION = '{}.wsgi.application'.format(PROJECT_NAME)

INSTALLED_APPS += (
    PROJECT_NAME,
    'enderecos',
    'pessoas'
)

ROOT_URLCONF = '{}.urls'.format(PROJECT_NAME)

if exists(join(BASE_DIR, 'logs')):
    DEBUG = False
    ALLOWED_HOSTS = ['*']

    HOST_NAME = ''
    DIGITAL_OCEAN_TOKEN = ''

    SERVER_EMAIL = 'root@djangoplus.net'
    ADMINS = [('Admin', 'root@djangoplus.net')]

    DROPBOX_TOKEN = ''

    INSTALLED_APPS += ('endless',)
else:
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = join(BASE_DIR, 'mail')

    INSTALLED_APPS += ('djangoplus.ui.themes.default',)


EXTRA_JS = ['/static/js/petshop.js', '/static/js/highlight.js']
EXTRA_CSS = ['/static/css/petshop.css', '/static/css/highlight.css']

DEFAULT_SUPERUSER = '000.000.000-00'
DEFAULT_PASSWORD = 'senha'
USERNAME_MASK = '000.000.000-00'

