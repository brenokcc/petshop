# -*- coding: utf-8 -*-

from django.conf import settings
from djangoplus.test import TestCase
from djangoplus.admin.models import User
from djangoplus.test.decorators import testcase


class AppTestCase(TestCase):
    AUDIO_FILE_PATH = '/Users/breno/Desktop/background.mp3'

    def test(self):
        User.objects.create_superuser(settings.DEFAULT_SUPERUSER, None, settings.DEFAULT_PASSWORD)
        self.execute_flow()

    @testcase('Cadastrando Doença')
    def cadastrar_doenca(self):
        self.click_menu('Catálogo', 'Doenças')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Sarna')
        self.click_button('Salvar')

    @testcase('Cadastrando Tipo de Procedimento')
    def cadastrar_tipoprocedimento(self):
        self.click_menu('Catálogo', 'Tipos de Procedimento')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Banho')
        self.enter('Valor', '23,00')
        self.click_button('Salvar')

    @testcase('Cadastrando Tipo de Animal')
    def cadastrar_tipoanimal(self):
        self.click_menu('Catálogo', 'Tipos de Animais')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Cão')
        self.click_button('Salvar')

    @testcase('Cadastrando Funcionário')
    def cadastrar_funcionario(self):
        self.click_menu('Funcionários')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Fernando Oliveira')
        self.enter('CPF', '213.501.594-68')
        self.choose('Sexo', 'Masculino')
        self.enter('Data de Nascimento', '27/08/1984')
        self.check('Endereço')
        self.enter('CEP', '59.141-000')
        self.enter('Número', '123')
        self.enter('Complemento', 'Apartamento 100')
        self.enter('Logradouro', 'Centro')
        self.enter('Bairro', 'Cohabinal')
        self.choose('Município', 'Natal')
        self.click_button('Salvar')

    @testcase('Cadastrando Cliente', username='213.501.594-68')
    def cadastrar_cliente(self):
        self.click_link('Clientes')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Bruna Santos')
        self.enter('CPF', '347.988.542-04')
        self.choose('Sexo', 'Feminino')
        self.enter('Data de Nascimento', '27/08/1984')
        self.check('Endereço')
        self.enter('CEP', '59.141-000')
        self.enter('Número', '123')
        self.enter('Complemento', 'Apartamento 100')
        self.enter('Logradouro', 'Centro')
        self.enter('Bairro', 'Cohabinal')
        self.choose('Município', 'Natal')
        self.click_button('Salvar')

    @testcase('Cadastrando Animal', username='213.501.594-68')
    def cadastrar_animal(self):
        self.click_link('Animais')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Bob')
        self.choose('Tipo', 'Cão')
        self.enter('Descrição', 'Animal dócil e bricalhão')
        self.choose('Proprietário', 'Bruna Santos')
        self.enter('Foto', '/static/images/bob.jpg')
        self.click_button('Salvar')

    @testcase('Iniciando Tratamento', username='213.501.594-68')
    def adicionar_tratamento_em_animal(self):
        self.click_link('Animais')
        self.click_icon('Visualizar')
        self.look_at_panel('Tratamentos')
        self.click_button('Iniciar Tratamento')
        self.look_at_popup_window()
        self.choose('Doença', 'Sarna')
        self.enter('Data de Início', '01/01/2018')
        self.click_button('Iniciar Tratamento')

    @testcase('Registrando Procedimento', username='213.501.594-68')
    def adicionar_procedimento_em_tratamento(self):
        self.click_link('Animais')
        self.click_icon('Visualizar')
        self.look_at_panel('Tratamentos')
        self.click_icon('Visualizar')
        self.look_at_panel('Procedimentos')
        self.click_button('Registrar Procedimento')
        self.look_at_popup_window()
        self.choose('Tipo', 'Banho')
        self.enter('Data/Hora', '01/01/2018 10:00')
        self.enter('Observação', '')
        self.click_button('Registrar Procedimento')

    @testcase('Finalizando Tratamento', username='213.501.594-68')
    def finalizar_em_tratamento(self):
        self.click_link('Animais')
        self.click_icon('Visualizar')
        self.look_at_panel('Tratamentos')
        self.click_icon('Visualizar')
        self.click_button('Finalizar Tratamento')
        self.look_at_popup_window()
        self.enter('Data de Término', '02/01/2018')
        self.click_button('Finalizar Tratamento')

    @testcase('Acompanhando Tratamento', username='347.988.542-04')
    def acompanhar_tratamento(self):
        self.look_at_panel('Meus Animais')
        self.click_button('Visualizar')
        self.look_at_panel('Tratamentos')
        self.click_icon('Visualizar')
